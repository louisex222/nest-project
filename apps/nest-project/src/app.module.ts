import { Module, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { LineNotifyModule } from 'nest-line-notify';
import { DateTime } from 'luxon';
import { ScraperModule } from './scraper/scraper.module';

import { TickerModule } from './ticker/ticker.module';

import { TickerService } from './ticker/ticker.service';

import { ScreenerModule } from './Screener/screener.module';

import { ScreenerService } from './Screener/Screener.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://localhost:27017/nest'),
    LineNotifyModule.forRoot({
      accessToken: process.env.LINE_NOTIFY_ACCESS_TOKEN,
    }),
    ScraperModule,

    TickerModule,

    ScreenerModule,
  ],
  providers: [TickerService, ScreenerService],
})
export class AppModule implements OnApplicationBootstrap {
  constructor(
    private readonly tickerService: TickerService,
    private readonly screenerService: ScreenerService,
  ) {}

  async onApplicationBootstrap() {
    Logger.log('正在初始化應用程式...', AppModule.name);

    const days = parseInt(process.env.SCRAPER_INIT_DAYS, 1) || 30;
    const startDate = DateTime.local().minus({ days });
    const endDate = DateTime.local();

    for (let dt = startDate; dt <= endDate; dt = dt.plus({ day: 1 })) {
      // await this.tickerService.updateTickers(dt.toISODate());
    }
    await this.screenerService.sendSelection();
    Logger.log('應用程式初始化完成', AppModule.name);
  }
}
